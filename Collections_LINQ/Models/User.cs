﻿using Newtonsoft.Json;
using System;

namespace Collections_LINQ.Models
{
    class User
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("firstName")]
        public string? FirstName { get; set; }

        [JsonProperty("lastName")]
        public string? LastName { get; set; }

        [JsonProperty("email")]
        public string? Email { get; set; }

        [JsonProperty("birthday")]
        public DateTime Birthday { get; set; }

        [JsonProperty("registeredAt")]
        public DateTime RegisteredAt { get; set; }

        [JsonProperty("teamId")]
        public int? TeamId { get; set; }

        public override string ToString()
        {
            return string.Format($"id:\t{Id}\n\t" +
                $"firstName:\t{FirstName ?? "This user has no first name"}\n\t" +
                $"lastName:\t{LastName ?? "This user has no last name"}\n\t" +
                $"email:\t{Email ?? "This user has no email"}\n\t" +
                $"birthday:\t{Birthday}\n\tregisteredAt:\t{RegisteredAt}");/* +
                $"teamId: {TeamId}"); ;*/
        }
    }
}
