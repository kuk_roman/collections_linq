﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Collections_LINQ.Models.FunctionModels
{
    class UserLastProjectTasks
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int? LastProjectTasksCount { get; set; }
        public int UndoneOrCanceledTasksCount { get; set; }
        public TaskModel LongestTask { get; set; }

    }
}
