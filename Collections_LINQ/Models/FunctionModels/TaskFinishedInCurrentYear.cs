﻿

namespace Collections_LINQ.Models.FunctionModels
{
    class TaskFinishedInCurrentYear
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
