﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Collections_LINQ.Models.FunctionModels
{
    class Project_Tasks
    {
        public Project Project { get; set; }
        public TaskModel LongestTaskByDescription { get; set; }
        public TaskModel ShortestTaskByName { get; set; }
        public int TotalUsersCount { get; set; }

    }
}
