﻿using Newtonsoft.Json;

namespace Collections_LINQ.Models
{
    class TaskStateModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("value")]
        public string? Value { get; set; }

        public override string ToString()
        {
            return string.Format($"id: {Id}\nvalue: {Value}");
        }
    }
}
