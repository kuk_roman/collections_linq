﻿using Newtonsoft.Json;
using System;

namespace Collections_LINQ.Models
{
    class TaskModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string? Name { get; set; }

        [JsonProperty("description")]
        public string? Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("finishedAt")]
        public DateTime FinishedAt { get; set; }

        [JsonProperty("state")]
        public TaskState State { get; set; }

        [JsonProperty("projectId")]
        public int ProjectId { get; set; }

        [JsonProperty("performerId")]
        public int PerformerId { get; set; }

        public User? Performer { get; set; }

        public override string ToString()
        {
            return string.Format($"\tid:\t{Id}\n\t" +
                $"name:\t{Name ?? "This task has no name"}\n\n\t" +
                $"description:\t{Description ?? "This task has no description"}\n\n\t" + 
                $"createdAt:\t{CreatedAt}\n\tfinishedAt:\t{FinishedAt}\n\tstate:\t{State}\n\t" +
                $"performer:\n\t[\n\t{Performer}\n\t]\n\n");
        }
    }
}
