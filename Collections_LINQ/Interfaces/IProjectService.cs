﻿using Collections_LINQ.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Collections_LINQ.Interfaces
{
    interface IProjectService
    {
        Task<ICollection<Project>> GetProjects();
        Task<Project> GetProjectById(int id);
    }
}
