﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Collections_LINQ.Interfaces
{
    interface ITaskService
    {
        Task<ICollection<Models.TaskModel>> GetTasks();
        Task<Models.TaskModel> GetTaskById(int id);
    }
}
