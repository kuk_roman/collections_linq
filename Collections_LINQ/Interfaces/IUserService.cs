﻿using Collections_LINQ.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Collections_LINQ.Interfaces
{
    interface IUserService
    {
        Task<ICollection<User>> GetUsers();
        Task<User> GetUserById(int id);
    }
}
