﻿using Collections_LINQ.Interfaces;
using Collections_LINQ.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ.Services
{
    class TaskStateService : ITaskStateService
    {
        private readonly string baseUrl;
        private readonly HttpClient client;

        public TaskStateService(string baseUrl, HttpClient client)
        {
            this.baseUrl = baseUrl;
            this.client = client;
        }

        public async Task<ICollection<TaskStateModel>> GetTaskStates()
        {
            var taskStates = await client.GetStringAsync($"{baseUrl}/api/taskStates");
            return JsonConvert.DeserializeObject<ICollection<TaskStateModel>>(taskStates);
        }
    }
}
