﻿using Collections_LINQ.Interfaces;
using Collections_LINQ.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ.Services
{
    class TaskService : ITaskService
    {
        private readonly string baseUrl;
        private readonly HttpClient client;

        public TaskService(string baseUrl, HttpClient client)
        {
            this.baseUrl = baseUrl;
            this.client = client;
        }

        public async Task<TaskModel> GetTaskById(int id)
        {
            var task = await client.GetStringAsync($"{baseUrl}/api/tasks/{id}");
            return JsonConvert.DeserializeObject<TaskModel>(task);
        }

        public async Task<ICollection<TaskModel>> GetTasks()
        {
            var tasks = await client.GetStringAsync($"{baseUrl}/api/tasks");
            return JsonConvert.DeserializeObject<ICollection<TaskModel>>(tasks);
        }
    }
}
