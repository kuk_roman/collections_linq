﻿using Collections_LINQ.Interfaces;
using Collections_LINQ.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ.Services
{
    class TeamService : ITeamService
    {
        private readonly string baseUrl;
        private readonly HttpClient client;

        public TeamService(string baseUrl, HttpClient client)
        {
            this.baseUrl = baseUrl;
            this.client = client;
        }

        public async Task<ICollection<Team>> GetTeams()
        {
            var teams = await client.GetStringAsync($"{baseUrl}/api/teams");
            return JsonConvert.DeserializeObject<ICollection<Team>>(teams);
        }

        public async Task<Team> GetTeamById(int id)
        {
            var team = await client.GetStringAsync($"{baseUrl}/api/teams/{id}");
            return JsonConvert.DeserializeObject<Team>(team);
        }
    }
}
