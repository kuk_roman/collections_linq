﻿using Collections_LINQ.Interfaces;
using Collections_LINQ.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ.Services
{
    class ProjectService: IProjectService
    {
        private readonly string baseUrl;
        private readonly HttpClient client;

        public ProjectService(string baseUrl, HttpClient client)
        {
            this.baseUrl = baseUrl;
            this.client = client;
        }

        public async Task<ICollection<Project>> GetProjects()
        {
            var projects = await client.GetStringAsync($"{baseUrl}/api/projects");
            return JsonConvert.DeserializeObject<ICollection<Project>>(projects);
        }

        public async Task<Project> GetProjectById(int id)
        {
            var project = await client.GetStringAsync($"{baseUrl}/api/projects/{id}");
            return JsonConvert.DeserializeObject<Project>(project);
        }
    }
}
