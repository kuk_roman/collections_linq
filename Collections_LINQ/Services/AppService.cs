﻿using Collections_LINQ.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;
using System.Net;
using Microsoft.AspNetCore.Http.Internal;
using Collections_LINQ.Models.FunctionModels;

namespace Collections_LINQ.Services
{
    class AppService
    {
        private readonly ProjectService _projectService;
        private readonly TaskService _taskService;
        private readonly TeamService _teamService;
        private readonly UserService _userService;
        private ICollection<Project> Projects { get; set; }
        private ICollection<TaskModel> Tasks { get; set; }
        private ICollection<Team> Teams { get; set; }
        private ICollection<User> Users { get; set; }

        public AppService(string baseUrl, HttpClient client)
        {
            _projectService = new ProjectService(baseUrl, client);
            _taskService = new TaskService(baseUrl, client);
            _teamService = new TeamService(baseUrl, client);
            _userService = new UserService(baseUrl, client);

            GetAllData();
        }

        public void GetAllData()
        {
            Teams = _teamService.GetTeams().Result.ToList();
            Users = _userService.GetUsers().Result.ToList();
            Tasks = _taskService.GetTasks().Result
                        .Join(Users,
                            task => task.PerformerId,
                            user => user.Id,
                            (task, user) =>
                            {
                                task.Performer = user;
                                return task;
                            })
                        .ToList();
            Projects = _projectService.GetProjects().Result
                .GroupJoin(Tasks,
                            project => project.Id,
                            task => task.ProjectId,
                            (pr, task) =>
                            {
                                pr.Tasks = task.ToList();
                                return pr;
                            })
                .Join(Teams,
                    project => project.TeamId,
                    team => team.Id,
                    (pr, team) =>
                    {
                        pr.Team = team;
                        return pr;
                    })
                .Join(Users,
                    project => project.AuthorId,
                    author => author.Id,
                    (pr, a) =>
                    {
                        pr.Author = a;
                        return pr;
                    })
                .ToList();
        }

        public Dictionary<Project, int> GetUserProjectTasksCountByUserId(int userId)
        {
            //var projects = Projects
            //    .Where(pr => pr.AuthorId == userId)
            //    .GroupJoin(Tasks,
            //            project => project.Id,
            //            task => task.ProjectId,
            //            (p, t) => new
            //            {
            //                Project = p,
            //                TasksCount = t.Count()
            //            })
            //    .ToDictionary(item => item.Project, item => item.TasksCount);

            var projects = Projects
                .Where(pr => pr.AuthorId == userId)
                .GroupBy(pr => pr)
                .ToDictionary(item => item.Key, item => item.Key.Tasks.Count());

            return projects;
        }

        public List<TaskModel> GetTasksByUserIdWithNameCondition(int userId)
        {
            var tasks = Tasks
                .Where(task => task.PerformerId == userId && task.Name.Length < 45)
                .ToList();

            return tasks;
        }

        public List<TaskFinishedInCurrentYear> GetTasksFinishedIn2020ByUserId(int userId)
        {
            var tasks = Tasks
                .Where(task => task.PerformerId == userId && 
                    task.FinishedAt.Year == DateTime.Now.Year/*2020*/ && 
                    task.State == TaskState.Finished)
                .Select(task => new TaskFinishedInCurrentYear{ Id = task.Id, Name = task.Name })
                .ToList();

            return tasks;
        }

        public List<Team_Users> GetTeamsUsersSortedByRegisteredDate()
        {
            var teams = Teams
                .GroupJoin(Users,
                    team => team.Id,
                    user => user.TeamId,
                    (t, u) => new Team_Users
                    {
                        Id = t.Id,
                        Name = t.Name,
                        Users = u.Where(us => DateTime.Now.Year - us.Birthday.Year > 10)
                            .OrderByDescending(us => us.RegisteredAt)
                            .ToList()
                    })
                .ToList();

            return teams;
        }

        public List<User_Tasks> GetUsersSortedByFirstNameAndSortedTasks()
        {
            var users = Users
                .GroupJoin(Tasks,
                    user => user.Id,
                    task => task.PerformerId,
                    (u, t) => new User_Tasks
                    {
                        User = u,
                        Tasks = t.OrderByDescending(task => task.Name.Length).ToList()
                    })
                .OrderBy(id => id.User.FirstName)
                .ToList();

            return users;
        }

        public List<UserLastProjectTasks> CreateNewUserStructure(int userId)
        {
            var structure = Users
                .Where(u => u.Id == userId)
                .GroupJoin(Projects,
                    user => user.Id,
                    project => project.AuthorId,
                    (us, pr) => new UserLastProjectTasks
                    {
                        User = us,
                        LastProject = pr
                            .OrderByDescending(p => p.CreatedAt)
                            .FirstOrDefault(),
                        LastProjectTasksCount = pr
                            .OrderByDescending(p => p.CreatedAt)
                            .FirstOrDefault()?
                            .Tasks.Count(),
                        UndoneOrCanceledTasksCount = Users
                            .Where(user => user.Id == userId)
                            .GroupJoin(Tasks,
                                user => user.Id,
                                task => task.PerformerId,
                                (u, t) =>
                                {
                                    return t.Where(task => task.State != TaskState.Finished).Count();
                                })
                            .FirstOrDefault(),
                        LongestTask = Users
                            .Where(user => user.Id == userId)
                            .GroupJoin(Tasks,
                                user => user.Id,
                                task => task.PerformerId,
                                (u, t) =>
                                {
                                    return t.OrderByDescending(task => task.FinishedAt - task.CreatedAt).FirstOrDefault();
                                })
                            .FirstOrDefault(),
                    }).ToList();

            return structure;
        }

        public List<Project_Tasks> CreateNewProjectStructure()
        {
            var structure = Projects
                .Join(Teams,
                    project => project.TeamId,
                    team => team.Id,
                    (pr, t) => new Project_Tasks
                    {
                        Project = pr,
                        LongestTaskByDescription = pr.Tasks
                            .OrderByDescending(task => task.Description.Length).FirstOrDefault(),
                        ShortestTaskByName = pr.Tasks
                            .OrderBy(task => task.Name.Length).FirstOrDefault(),
                        TotalUsersCount = Teams
                        .Where(team => team.Id == t.Id)
                        .GroupJoin(Users,
                            team => team.Id,
                            user => user.TeamId,
                            (t, u) =>
                            {
                                if (pr.Description.Length > 20 || pr.Tasks.Count() < 3)
                                    return u.Count();
                                else
                                    return -1;
                            })
                        .FirstOrDefault()
                    })
                .ToList();
            return structure;
        }

        public void PrintUserProjectTasksCountByUserId(int userId)
        {
            var tasks = (GetUserProjectTasksCountByUserId(userId));
            if(tasks.Count == 0)
            {
                Console.WriteLine("User is not an author of any project");
                return;
            }
            foreach (var t in tasks)
            {
                Console.WriteLine($"Project:\n[\n{t.Key}\n]\n\nTaskCount:\t{t.Value}\n" +
                $"#########################################\n");
            }
        }

        public void PrintTasksByUserIdWithNameCondition(int userId)
        {
            var tasks = (GetTasksByUserIdWithNameCondition(userId));
            if(tasks.Count == 0)
            {
                Console.WriteLine("User is not a performer of any task");
                return;
            }
            Console.WriteLine("Tasks:\n[");
            foreach (var t in tasks)
            {
                Console.WriteLine(t.ToString());
                Console.WriteLine("#################################\n");
            }
            Console.WriteLine("]\n");
        }

        public void PrintTasksFinishedIn2020ByUserId(int userId)
        {
            var tasks = GetTasksFinishedIn2020ByUserId(userId);
            if (!tasks.Any())
            {
                Console.WriteLine("No tasks of this user has been finished in 2020\n");
                return;
            }
            foreach (var t in tasks)
            {
                Console.WriteLine($"id:\t{t.Id}\nname:\t{t.Name}\n");
            }
        }

        public void PrintTeamsUsersSortedByRegisteredDate()
        {
            var teams = GetTeamsUsersSortedByRegisteredDate();
            foreach(var t in teams)
            {
                Console.WriteLine($"id:\t{t.Id}\nname:\t{t.Name}\nusers:\n[");
                if (t.Users.Count() == 0)
                {
                    Console.WriteLine("\tNo Users satisfying the condition");
                }
                else
                {
                    foreach (var user in t.Users)
                    {
                        Console.WriteLine("\t" + user.ToString() + "\n");
                    }
                }
                Console.Write("]\t\n\n");
            }
        }

        public void PrintUsersSortedByFirstNameAndSortedTasks()
        {
            var users = GetUsersSortedByFirstNameAndSortedTasks();
            foreach (var u in users)
            {
                Console.WriteLine($"user:\n[\n\t{u.User}\n]\ntasks:\n[");
                if (u.Tasks.Count() == 0)
                {
                    Console.WriteLine("\tThis user has no tasks\n");
                }
                else {
                    foreach (var task in u.Tasks)
                    {
                        Console.WriteLine(task.ToString());
                    }
                }

                Console.Write("]\t\n##############################\n");
            }
        }

        public void PrintNewUserStructure(int userId)
        {
            var structure = CreateNewUserStructure(userId);
            foreach (var s in structure)
            {
                Console.WriteLine($"user:\n[\n\t{s.User}\n]\n\n" +
                    $"last project:\n[\n\t{(s.LastProject == null ? "This user has no project" : s.LastProject.ToString())}\n]\n\n" +
                    $"total tasks count of last project:\t" +
                    $"{(s.LastProjectTasksCount.HasValue ? s.LastProjectTasksCount.ToString() : "As this user has no project, the user has no task of it")}\n\n" +
                    $"undone or canceled tasks count:\t{s.UndoneOrCanceledTasksCount}\n\n" +
                    $"longest task:\n[\n{s.LongestTask}\n]\n##############################\n");
            }
        }

        public void PrintNewProjectStructure()
        {
            var structure = CreateNewProjectStructure();
            foreach (var s in structure)
            {
                Console.WriteLine($"project:\n[\n\t{s.Project}\n]\n\n" +
                    $"the longest project task by descriprion:\n[\n{s.LongestTaskByDescription}\n]\n\n" +
                    $"the shortest project task by name:\n[\n{s.ShortestTaskByName}\n]\n\n" +
                    $"total users count in project team:\t{s.TotalUsersCount}\n" +
                    "##############################\n");
            }
        }

        public bool IsUserExist(int userId)
        {
            if (!Users.Where(user => user.Id == userId).Any())
            {
                Console.WriteLine("User with such id doesn't exist!");
                return false;
            }
            return true;
        }
    }
}
