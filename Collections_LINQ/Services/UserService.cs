﻿using Collections_LINQ.Interfaces;
using Collections_LINQ.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ.Services
{
    class UserService : IUserService
    {
        private readonly string baseUrl;
        private readonly HttpClient client;

        public UserService(string baseUrl, HttpClient client)
        {
            this.baseUrl = baseUrl;
            this.client = client;
        }

        public async Task<ICollection<User>> GetUsers()
        {
            var users = await client.GetStringAsync($"{baseUrl}/api/users");
            return JsonConvert.DeserializeObject<ICollection<User>>(users);
        }

        public async Task<User> GetUserById(int id)
        {
            var user = await client.GetStringAsync($"{baseUrl}/api/users/{id}");
            return JsonConvert.DeserializeObject<User>(user);
        }
    }
}
